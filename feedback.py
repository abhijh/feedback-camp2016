from flask import Flask, render_template
from flask import request,redirect
from flask_sqlalchemy import SQLAlchemy
 
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///feedback.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)

class Feedback(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    content = db.Column(db.Text)

    def __init__(self, name, content):
        self.content = content
        self.name = name

    def __repr__(self):
        return 'Feedback by : %s \ncontent is : %s' % (self.name, self.content)


db.create_all()

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/submit', methods=['POST'])
def submit():
	name = request.values.get("name", None)
	if not name:
		name = "Anonymous"
	content = request.values.get("content", None)
	feedback = Feedback(name, content)
	db.session.add(feedback)
	db.session.commit()
	print feedback
	return redirect("/")


if __name__ == '__main__':
	app.run(debug = True)
